<?php
error_reporting(E_ALL);

if(!defined('ABS_PATH')) define('ABS_PATH', realpath($_SERVER["DOCUMENT_ROOT"]));
require (ABS_PATH.'/models/notes_model.php');
$sn = new SimpleNotesClass();

if(!isset($_POST['action'])){
	$resp = array('success' => 0, 'msg' => 'Action not specified.');
	echo json_encode($resp);
	exit();
}
$action = filter_var($_POST['action'], FILTER_SANITIZE_STRING);

if($action == 'show_table'){
	// Filter input and deal with pagination
	$sort_type = filter_var($_POST['sort_type'], FILTER_SANITIZE_STRING);
	$sort_asc = filter_var($_POST['sort_asc'], FILTER_SANITIZE_NUMBER_INT);
	$page_num = intval(filter_var($_POST['page_num'], FILTER_SANITIZE_NUMBER_INT));
	$per_page = 5; // Using a small value (5) for demo purposes
	$offset = max(0, ($page_num - 1) * $per_page);

	// Get data from model
	$total_items = $sn->get_total();
	$notes = $sn->get_notes($sort_type, $sort_asc, $offset, $per_page);

	$resp = array('success' => 1, 'perPage' => $per_page, 'pageNum' => $page_num, 'totalItems' => $total_items, 'results' => $notes);
	echo json_encode($resp);
	exit();
}
else if($action == 'refresh_row'){
	$note_id = filter_var($_POST['note_id'], FILTER_SANITIZE_NUMBER_INT);

	if($note_id){
		$note = $sn->get_note($note_id);
	}else{
		$note = null;
	}
	$not_found = empty($note) ? 1 : 0;

	$resp = array('success' => 1, 'resultRow' => $note, 'notFound' => $not_found);
	echo json_encode($resp);
	exit();
}
else if($action == 'show_note_details'){
	$note_id = filter_var($_POST['note_id'], FILTER_SANITIZE_NUMBER_INT);

	if(!$note_id){
		$note = array('id' => 0, 'title' => '', 'body' => '', 'date' => date('m/d/Y'));
	}else{
		$note = $sn->get_note($note_id);
	}

	if(empty($note)){
	?>
		Note not found.
	<?php
	}else{
	?>
		<form class="clearfix" style="width:800px">
			<input id="note_id" type="hidden" value="<?= $note['id'] ?>" autofocus />
			<div class="wrapper_3">
				<label>Date</label><br />
				<input id="note_date" type="text" value="<?= date('m/d/Y', strtotime($note['date'])) ?>" />
			</div>
			<div class="wrapper_7">
				<label>Title</label><br />
				<input id="note_title" type="text" value="<?= $note['title'] ?>" maxlength="255" pattern="^[A-Za-z\.\s]+$" required />
			</div>
			<div class="wrapper_10 clearer">
				<label>Note</label><br />
				<textarea id="note_body" rows="6"><?= $note['body'] ?></textarea>
			</div>
			<div class="clearer"></div>
			<input type="button" class="bigger_input" value="Save" onclick="notesController.updateNoteDetails();" />
			<input type="button" class="bigger_input" value="Cancel" onclick="notesController.cancelNoteDetails();" />
		</form>
	<?php
	}
}
else if($action == 'update_note_details'){
	$note_id = filter_var($_POST['note_id'], FILTER_SANITIZE_NUMBER_INT);
	$note_date = date('Y-m-d', strtotime(filter_var($_POST['note_date'], FILTER_SANITIZE_STRING)));
	$note_title = preg_replace('/[^A-Za-z\.\s]/', '', $_POST['note_title']);
	$note_body = filter_var($_POST['note_body'], FILTER_SANITIZE_SPECIAL_CHARS);

	if(trim($note_title) == ''){
		$resp = array('success' => 0, 'msg' => 'Please enter a title for the note.');
		echo json_encode($resp);
		exit();
	}
	if(!$note_id){
		$result = $sn->insert_note($note_date, $note_title, $note_body);
	}else{
		$result = $sn->update_note($note_id, $note_date, $note_title, $note_body);
	}

	if(!$result){
		$resp = array('success' => 0, 'msg' => 'Something went wrong...');
		echo json_encode($resp);
		exit();
	}
	$resp = array('success' => 1);
	echo json_encode($resp);
	exit();
}
else if($action == 'delete_note'){
	$note_id = filter_var($_POST['note_id'], FILTER_SANITIZE_NUMBER_INT);

	// Delete note
	$affected = $sn->delete_note($note_id);
	if(!$affected){
		$resp = array('success' => 0, 'msg' => 'Note not found.');
		echo json_encode($resp);
		exit();
	}

	echo json_encode(array('success' => 1));
	exit();
}
else{
	$resp = array('success' => 0, 'msg' => 'Action not specified.');
	echo json_encode($resp);
	exit();
}
?>