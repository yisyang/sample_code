<?php
error_reporting(E_ALL);
require(realpath($_SERVER["DOCUMENT_ROOT"]).'/config/dbconn.php');
$sql = "CREATE TABLE IF NOT EXISTS `notes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `body` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1";
$db->query($sql);

$sql = "DESCRIBE notes";
$results = $db->query($sql)->fetchAll(PDO::FETCH_ASSOC);
foreach($results as $result){
    echo $result['Field'] .' - '. $result['Type'] .'<br />';
}

header("Refresh:3;url=index.php");
exit('Page will redirect in 3 seconds.');
?>