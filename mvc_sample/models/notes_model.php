<?php
require_once(ABS_PATH.'/config/dbconn.php');

// Simple Notes Class
class SimpleNotesClass{
	protected $db;
	public function __construct(){
		$this->db = simpleDBConn::connect('main');
	}
	public function get_total(){
		$sql = "SELECT COUNT(*) from notes";
		return $this->db->query($sql)->fetchColumn();
	}
	public function get_notes($sort_type, $sort_asc, $offset = 0, $per_page = 5){
		$sort_type_query = 'date';
		$sort_options = array('title', 'body', 'date');
		if(in_array($sort_type, $sort_options)) $sort_type_query = $sort_type;
		$sort_asc_query = 'DESC';
		if($sort_asc == 1) $sort_asc_query = 'ASC';
		$sql = "SELECT * FROM notes ORDER BY $sort_type_query $sort_asc_query LIMIT $offset, $per_page";
		return $this->db->query($sql)->fetchAll(PDO::FETCH_ASSOC);
	}
	public function get_note($note_id){	
		$sql = "SELECT * FROM notes WHERE id = '$note_id'";
		return $this->db->query($sql)->fetch(PDO::FETCH_ASSOC);
	}
	public function insert_note($note_date, $note_title, $note_body){
		$query = $this->db->prepare("INSERT INTO notes (date, title, body) VALUES (:date, :title, :body)");
		$result = $query->execute(array(':date' => $note_date, ':title' => $note_title, ':body' => $note_body));
		return $result;
	}
	public function update_note($note_id, $note_date, $note_title, $note_body){
		$query = $this->db->prepare("UPDATE notes SET date = :date, title = :title, body = :body WHERE id = :id");
		$result = $query->execute(array(':id' => $note_id, ':date' => $note_date, ':title' => $note_title, ':body' => $note_body));
		return $result;
	}
	public function delete_note($note_id){
		$query = $this->db->prepare("DELETE FROM notes WHERE id = :id");
		$query->execute(array(':id' => $note_id));
		return $query->rowCount();
	}
}
?>