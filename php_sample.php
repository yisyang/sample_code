<?php
/*
	PHP CODE SAMPLES
	Yi Su Yang
	2013-06-19
	
	INDEX
	Line #		Description
	016			A short language switcher function
	035			The set locale portion to match the above sample
	044			Excerpt from a controller file on a non-framework based custom site
	075			Excerpt from a codeIgnitor controller
	114			Excerpt from a codeIgnitor model
*/

// A short language switcher function
session_start();
if(isset($_POST['q']) && $_POST['q'] !== ''){
	$q = filter_var($_POST['q'], FILTER_SANITIZE_STRING);
	$q_options = array('en-US', 'es-MX', 'fr-FR', 'zh-CN', 'zh-TW');
	if(in_array($q, $q_options)){
		$_SESSION['lang'] = $q;
		$resp = array('success' => 1);
		echo json_encode($resp);
		exit();
	}else{
		$resp = array('success' => 0, 'msg' => 'Invalid Language');
		exit();
	}
}else{
	$resp = array('success' => 0, 'msg' => 'No Language Given');
	exit();
}

// The set locale portion to match the above sample
$locale = isset($_SESSION['lang']) ? $_SESSION['lang'] : 'en-US';
$locale_f = preg_replace('/[-]+/', '_', $locale);
putenv("LC_ALL=$locale_f");
setlocale(LC_ALL, $locale_f);
bindtextdomain("default", realpath($_SERVER["DOCUMENT_ROOT"])."/locale");
bind_textdomain_codeset("default", 'UTF-8');
textdomain("default");

// Excerpt from a controller file on a non-framework based custom site
if($action == 'delete_property'){
	$property_id = filter_var($_POST['property_id'], FILTER_SANITIZE_NUMBER_INT);

	// Delete property listing
	$query = $db->prepare("DELETE FROM properties WHERE id = :pid");
	$query->execute(array(':pid' => $property_id));
	$affected = $query->rowCount();
	if(!$affected){
		$resp = array('success' => 0, 'msg' => 'Property not found.');
		echo json_encode($resp);
		exit();
	}

	// Delete all image files belonging to the listing
	$sql = "SELECT filename FROM properties_images WHERE property_id = $property_id";
	$img_files = $db->query($sql)->fetchAll(PDO::FETCH_ASSOC);
	foreach($img_files as $img_file){
		$filename = $img_file['filename'];
		@unlink('../img/properties/'.$filename);
		@unlink('../img/properties/thumbs/'.$filename);
	}

	// Finally remove the images in the db
	$sql = "DELETE FROM properties_images WHERE property_id = $property_id";
	$db->query($sql);

	echo json_encode(array('success' => 1));
	exit();
}

// CodeIgnitor Controller Sample
class Bucket extends Custom_Controller{
	function __construct(){
		parent::__construct();
		if(!sess_var('logged_in')){
			redirect('login');
		}
		$this->load->model('buckets_model');
	}
	function fill(){
		$params = $this->uri->uri_to_assoc(3);
		if(!isset($params['bucket_id']) || !$params['bucket_id']){
			$resp = array('success' => 0, 'msg' => 'Bucket ID is missing.');
			echo json_encode($resp);
			return;
		}
		if(!isset($params['water_vol']) || $params['water_vol'] < 0 || $params['water_vol'] > 100){
			$resp = array('success' => 0, 'msg' => 'Water volume is missing or is invalid.');
			echo json_encode($resp);
			return;
		}
		$new_water_vol = $this->buckets_model->fill_bucket($params['bucket_id'], $params['water_vol']);
		$resp = array('success' => 1, 'bucket_id' => $params['bucket_id'], 'water_vol' => $new_water_vol);
		echo json_encode($resp);
		return;
	}
	function empti($bucket_id=0){
		if(!$bucket_id){
			$resp = array('success' => 0, 'msg' => 'Bucket ID is missing.');
			echo json_encode($resp);
			return;
		}
		$this->buckets_model->empty_bucket($bucket_id);
		$resp = array('success' => 1, 'bucket_id' => $bucket_id, 'water_vol' => 0);
		echo json_encode($resp);
		return;
	}
}

// CodeIgnitor Model Sample
class Buckets_model extends CI_Model {
	function fill_bucket($bucket_id, $water_vol){
		// Add water
		$sql = "UPDATE buckets SET water_vol = water_vol + ? WHERE id = ?";
		$this->db->query($sql, array($water_vol, $bucket_id));
		
		// Return new water level
		$new_water_vol = $this->db->get_where('buckets', array('id', $bucket_id))->row()-> water_vol;
		return $new_water_vol;
	}
	function empty_bucket($bucket_id){
		$this->db->where('id', $bucket_id)
		->set('water_vol', 0)
		->update('buckets');
		return 1;
	}
}
?>