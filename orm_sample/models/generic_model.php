<?php
require_once(ABS_PATH.'/models/generic_orm.php');

// Generic Model
abstract class GenericModel{
	public $db;
	public $table_name;
	public $table_columns = array();
	public $primary_key;
	public $old_data;

	public function __construct(){
		$this->db = simpleDBConn::connect('main');
		GenericORM::mapMe($this->table_name);
	}
	public function all(){
		$results = GenericORM::getTableData();
		return $results;
	}
	public function count($params = null){
		if(empty($params) || $params == 'all'){
			$result = GenericORM::getCount();
		}else{
			$result = GenericORM::getCount($params);
		}
		return $result;
	}
	public function find($params = null, $sort_query = null, $limit_query = null){
		if(empty($params) || $params == 'all'){
			$results = GenericORM::getTableData(null, $sort_query, $limit_query);
		}else if(is_numeric($params)){
			$params_by_pkey = array($this->primary_key => $params);
			$results = GenericORM::getTableData($params_by_pkey, $sort_query, $limit_query);
		}else{
			$results = GenericORM::getTableData($params, $sort_query, $limit_query);
		}
		return $results;
	}
	public function save(){
		if(empty($this->old_data) || is_null($this->old_data->{$this->primary_key})){
			$success = GenericORM::insertTableData();
		}else{
			$success = GenericORM::updateTableData();
		}
		return $success;
	}
	public function delete($params = null){
		if(is_numeric($params)){
			$params_by_pkey = array($this->primary_key => $params);
			$success = GenericORM::deleteTableData($params_by_pkey);
		}else{
			$success = GenericORM::deleteTableData($params);
		}
		return $success;
	}
}
?>