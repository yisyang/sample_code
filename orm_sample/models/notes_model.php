<?php
require_once(ABS_PATH.'/models/generic_model.php');

// Simple Notes ORM
class Note extends GenericModel{
	public function __construct(){
		$this->table_name = 'notes';
		parent::__construct();
	}
}
?>