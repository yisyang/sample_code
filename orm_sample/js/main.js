// jqDialog
var modalController = {
	overlayClickable: true
}
jQuery(document).ready(function(){
	jQuery(document).on('click', '.jqDialog', function(event){
		event.preventDefault();
		var paramsUrl = jQuery(event.currentTarget).attr('params');
		if(typeof(paramsUrl) === 'undefined'){
			jqDialogInit(jQuery(event.currentTarget).attr('href'));
		}else{
			var pair, params = {};
			var pairs = paramsUrl.split('&');
			for(var i = 0; i < pairs.length; i++){
				pair = pairs[i].split('=');
				params[pair[0]] = pair[1];
			}
			jqDialogInit(jQuery(event.currentTarget).attr('href'), params);
		}
	});
	jQuery(document).on('click', '.jqDialog-close-btn', function(event){
		jQuery('#jq-dialog-modal').dialog('close');
	});
	jQuery(document).on('click', '.ui-widget-overlay', function(){
		if(modalController.overlayClickable && jQuery('#jq-dialog-modal').hasClass("ui-dialog-content")) jQuery("#jq-dialog-modal").dialog('close'); 
	});
});

// Shortcut function to initiate dialogs
function jqDialogInit(url, params, callback){
	if(typeof(url) === 'undefined' || url == "") return false;
	if(typeof(params) === 'undefined') params = {};
	jQuery.ajax({
		type: "POST", 
		url: url,
		data: params,
		dataType: "html",
		success: function(data){
			jQuery('#jq-dialog-modal').html(data).dialog({
				modal: true,
				width: 840,
				zIndex: 9900,
				resizable: false,
				open: function(){
					jQuery('#jq-dialog-modal a').blur();
					jQuery('#jq-dialog-modal .bigger_input').blur();
				},
				beforeClose: function(){
					modalController.overlayClickable = true;
				}
			});
			if(typeof(callback) === 'function'){
				callback();
			}
			// Position fix for moving from one screen to another
			if(jQuery('#jq-dialog-modal').dialog().height() > jQuery(window).height()){
				jQuery('.ui-dialog').css('top', '0');
				jQuery('.ui-dialog').get(0).scrollIntoView();
			}else if(parseInt(jQuery('.ui-dialog').css('top')) < jQuery(window).scrollTop()){
				jQuery('.ui-dialog').get(0).scrollIntoView();
			}
		},
		error: function(xhr, ajaxOptions, thrownError){ alert(xhr.responseText); }
	});
}

// Simple alert and confirm dialogs
function jAlert(msg){
	jQuery('#jq-dialog-msg').html(msg).dialog({
		modal: true,
		resizable: false,
		buttons: {
			Close: function(){
				$(this).dialog("close");
			}
		}
	});
}
function jConfirm(msg, callback){
	jQuery('#jq-dialog-msg').html('<img style="float:left;padding: 0 8px 6px 0;" src="/img/warning.png" />' + msg).dialog({
		modal: true,
		resizable: false,
		buttons: {
			Confirm: function(){
				$(this).dialog("close");
				callback();
			},
			Close: function(){
				$(this).dialog("close");
			}
		}
	});
}

// User interaction portion of the notes program
var notesController = {
	ajax: function(params, callback){
		jQuery.ajax({
			type: "POST", 
			url: "/controllers/notes_controller.php",
			data: params,
			dataType: "json",
			success: function(resp){
				if(resp.success){
					if(typeof(callback) === 'function'){
						callback(resp);
					}
				}else{
					if(typeof(resp.msg) !== 'undefined' && resp.msg){
						jAlert(resp.msg);
					}
				}
			},
			error: function(xhr, ajaxOptions, thrownError){ console.log(xhr.responseText); }
		});
	},
	showTable: function(pageNum, sortType, sortAsc, noScroll){
		if(typeof(noScroll) === 'undefined') noScroll = 0;
		var params = {action: 'show_table', page_num: pageNum, sort_type: sortType, sort_asc: sortAsc};
		this.ajax(params, function(resp){
			var totalPages = Math.ceil(resp.totalItems / resp.perPage);
			notesController.showMenu(resp.pageNum, totalPages, "'" + sortType + "', '" + sortAsc + "'");
			var table = '\
				<thead> \
					<tr> \
						<th>';
			if(sortType == 'date' && sortAsc == 1){
				table += '<a class="sorter" onclick="notesController.showTable(1, \'date\', 0)">Date</a>';
			}else{
				table += '<a class="sorter" onclick="notesController.showTable(1, \'date\', 1)">Date</a>';
			}
			table += '</th> \
						<th>';
			if(sortType == 'title' && sortAsc == 1){
				table += '<a class="sorter" onclick="notesController.showTable(1, \'title\', 0)">Title</a>';
			}else{
				table += '<a class="sorter" onclick="notesController.showTable(1, \'title\', 1)">Title</a>';
			}
			table += '</th> \
						<th>';
			if(sortType == 'body' && sortAsc == 1){
				table += '<a class="sorter" onclick="notesController.showTable(1, \'body\', 0)">Note</a>';
			}else{
				table += '<a class="sorter" onclick="notesController.showTable(1, \'body\', 1)">Note</a>';
			}
			table += '</th> \
						<th>Actions</th> \
					</tr> \
				</thead> \
				<tbody>';
			if(resp.results.length){
				for(var i in resp.results){
					var note_result = resp.results[i];
					table += '\
					<tr id="note_display_' + note_result.id + '" class="note_tr" note_id="' + note_result.id + '"> \
						<td style="text-align:center;">' + moment(note_result.date).format('MM/DD/YYYY') + '</td> \
						<td style="text-align:center;">' + note_result.title + '</td> \
						<td style="text-align:center;"><span class="note_body">' + note_result.body + '</span></td> \
						<td><a style="cursor:pointer;" onclick="notesController.showNoteDetails(' + note_result.id + ');" title="Edit Note"><img src="/img/edit.gif" alt="[Edit]" /></a> <a style="cursor:pointer;" onclick="notesController.deleteNote(' + note_result.id + ');" title="Delete Note"><img src="/img/delete.gif" alt="[Delete]" /></a></td> \
					</tr>';
				}
			}else{
				table += '\
					<tr><td colspan="4">No notes found.</td></tr>';
			}
			table += '\
				</tbody>';
			document.getElementById('notes_table').style.display = '';
			jQuery('#notes_table').html(table);
			if(!noScroll){
				document.getElementById('notes_top_nav').scrollIntoView();
			}
		});
	},
	refreshRow: function(noteId){
		var params = {action: 'refresh_row', note_id: noteId};
		notesController.ajax(params, function(resp){
			if(resp.notFound == 1){
				jQuery("#note_display_" + noteId).fadeOut(500, function(){
					jQuery("#note_display_" + noteId).html('<td colspan="4"><b><i>LISTING DELETED</i></b></td>').fadeIn(500);
				});
			}else{
				var note_row = '';
				var note_result = resp.resultRow;
				note_row += '\
						<td style="text-align:center;">' + moment(note_result.date).format('MM/DD/YYYY') + '</td> \
						<td style="text-align:center;">' + note_result.title + '</td> \
						<td style="text-align:center;"><span class="note_body">' + note_result.body + '</span></td> \
						<td><a style="cursor:pointer;" onclick="notesController.showNoteDetails(' + note_result.id + ');" title="Edit Note"><img src="/img/edit.gif" alt="[Edit]" /></a> <a style="cursor:pointer;" onclick="notesController.deleteNote(' + note_result.id + ');" title="Delete Note"><img src="/img/delete.gif" alt="[Delete]" /></a></td>';
				jQuery("#note_display_" + noteId).fadeOut(500, function(){
					jQuery("#note_display_" + noteId).html(note_row).fadeIn(500);
				});
			}
		});
	},
	showMenu: function(pageNum, totalPages, fixedParams, tableHandler){
		var nav = '';
		var page;
		if(typeof(tableHandler) === 'undefined') tableHandler = 'showTable';
		if(totalPages > 1){
			if(pageNum > 1){
				page  = pageNum - 1;
				nav += '<div class="table_nav"> <a onclick="notesController.' + tableHandler + '(' + page + ', ' + fixedParams + ')">Prev</a> ';
			}else{
				nav += '<div class="table_nav"> <a class="disabled">Prev</a> ';
			}
			for(var i = 1; i <= totalPages; i++){
				if(i != pageNum){
					nav += ' <a onclick="notesController.' + tableHandler + '(' + i + ', ' + fixedParams + ')">' + i + '</a> ';
				}else{
					nav += ' <a class="selected" onclick="notesController.' + tableHandler + '(' + i + ', ' + fixedParams + ')">' + i + '</a> ';
				}
			}
			if (pageNum < totalPages){
				page = pageNum + 1;
				nav += ' <a onclick="notesController.' + tableHandler + '(' + page + ', ' + fixedParams + ')">Next</a> </div>';
			}else{
				nav += ' <a class="disabled">Next</a> </div>';
			}
		}
		jQuery('.note_nav_container').html(nav);
	},
	showNoteDetails: function(noteId){
		var params = {action: 'show_note_details', note_id: noteId};
		modalController.overlayClickable = false;
		jqDialogInit('/controllers/notes_controller.php', params, function(){
			$("#note_date").datepicker().blur();
		});
	},
	updateNoteDetails: function(){
		var noteId = document.getElementById('note_id').value;
		var noteTitle = document.getElementById('note_title').value;
		var noteBody = document.getElementById('note_body').value;
		var noteDate = document.getElementById('note_date').value;

		if(noteTitle == ''){
			jAlert('Please write a title for this note.');
			$('#note_title').focus;
			return false;
		}
		if(noteTitle.match(/[^A-Za-z\.\s]/)){
			jAlert('Sorry, only letters, periods, and spaces are allowed in the title.');
			$('#note_title').focus;
			return false;
		}
		
		var params = {action: 'update_note_details', note_id: noteId, note_date: noteDate, note_title: noteTitle, note_body: noteBody};
		this.ajax(params, function(resp){
			if(noteId != '0'){
				notesController.refreshRow(noteId);
				jQuery('#jq-dialog-modal').dialog('close');
			}else{
				notesController.showTable(1, 'date', 0, 1);
				jQuery('#jq-dialog-modal').dialog('close');
			}
		});
	},
	cancelNoteDetails: function(){
		var noteTitle = document.getElementById('note_title').value;
		var noteBody = document.getElementById('note_body').value;
		if(noteTitle != '' || noteBody != ''){
			jConfirm('Are you sure? All changes will be lost.', function(){
				jQuery('#jq-dialog-modal').dialog('close');
			});
		}else{
			jQuery('#jq-dialog-modal').dialog('close');
		}
	},
	deleteNote: function(noteId){
		jConfirm('Are you sure? This note will be PERMANENTLY deleted!', function(){
			var params = {action: 'delete_note', note_id: noteId};
			notesController.ajax(params, function(resp){
				jQuery("#note_display_" + noteId).remove();
				if($('#notes_table tbody').children('tr').length == 0){
					$('#notes_table').html('<tr><td colspan="4">No notes found.</td></tr>');
				}
			});
		});
	},
}