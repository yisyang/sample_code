<?php
/*	
* Simple ORM by Scott Yang, 7/12/2013, on PHP 5.4
*
* WARNING: Sample Only
* This is a sample created in less than a day and is absolutely not suitable for production 
* due to its lack of security mechanisms and practical features
*/

if($_SERVER["SERVER_NAME"] == "localhost"){
	error_reporting(E_ALL);
}else{
	error_reporting(E_STRICT);
}
if(!defined('ABS_PATH')) define('ABS_PATH', realpath($_SERVER["DOCUMENT_ROOT"]));

// Include header
require(ABS_PATH.'/layout/header.php');

// Actual notes view
?>

<div id="add_note_btn"><img src="/img/add.gif" /> Add Note</div>

<div id="notes_top_nav" class="note_nav_container"></div>
<table id="notes_table" class="default_table opaque">
	<tbody>
		<tr>
			<td>Loading...<noscript>Please enable javascript for this site to function.</noscript></td>
		</tr>
	</tbody>
</table>
<div class="note_nav_container"></div>

<script>
	$(document).ready(function(){
		$('#add_note_btn').on('click', function(){
			notesController.showNoteDetails(0); // 0 for new notes
		});
		notesController.showTable(1, 'date', 0, 1);
	});
</script>

<?php
// Include footer
require(ABS_PATH.'/layout/footer.php');

?>