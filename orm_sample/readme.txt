ORM Sample
Scott Yang, 07/12/2013
This sample is fully functional and is created for a one-day test to write a functional ORM.

Simply create a vhost on localhost and put code under the doc root for the vhost, 
add a database with user/pass/db all set to simple_notes,
then run create_db.php to begin.