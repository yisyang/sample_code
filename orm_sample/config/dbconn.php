<?php
// Simple DB connector, with credentials defined inline
class simpleDBConn{
	private static $dbhost;
	private static $dbuser;
	private static $dbpass;
	private static $dbname;
	private static $environment;
	public static function config(){
		static::$dbhost['dev'] = 'localhost';
		static::$dbuser['dev'] = 'simple_notes';
		static::$dbpass['dev'] = 'simple_notes';
		static::$dbname['dev']['main'] = 'simple_notes';

		static::$dbhost['prod'] = 'localhost';	// May need to use outside domain/IP for some server configs
		static::$dbuser['prod'] = 'remote_host';
		static::$dbpass['prod'] = 'remote_pass';
		static::$dbname['prod']['main'] = 'simple_notes';

		if(!static::$environment){
			if($_SERVER["SERVER_NAME"] == "localhost"){
				static::$environment = 'dev';
			}elseif($_SERVER["SERVER_NAME"] == "www.remote-host.com" || $_SERVER["SERVER_NAME"] == "123.45.67.89"){
				static::$environment = 'prod';
			}else{
				die("DB Login is not configured, quitting...");
			}
		}
	}
	public static function connect($db_name = 'main', $environment = null){
		static::config();
		if(is_null($environment)) $environment = static::$environment;

		// Undefined DB, something is wrong
		if(!isset(static::$dbname[$environment][$db_name])) die("DB Name is not configured, quitting...");
		
		$db = new PDO('mysql:host='. static::$dbhost[$environment]. ';dbname='. static::$dbname[$environment][$db_name] .';charset=utf8', static::$dbuser[$environment], static::$dbpass[$environment]) or die ('Error connecting to DB using PDO');
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
		$db->query("SET time_zone = 'America/Los_Angeles'");
		$db->query("SET CHARACTER SET utf8");

		return $db;
	}
}
?>